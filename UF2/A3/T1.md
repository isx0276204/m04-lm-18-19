Transformar documents XML
=========================

MP4UF2A3T1

Simples canvis en documents XML

Processament de documents XML
-----------------------------

Els documents XML poden ser tractats com la entrada de programes, i
aquests els poden generar com a sortida. En aquesta tasca farem simples
programes que transformen petits detalls dels documents XML o que
extrauen informacions dels mateixos. Els programes tindran en general
una estructura similar a aquest, que simplement converteix a minúscules
els noms dels elements:

    #!/usr/bin/python
    #
    # Transform tag names to lowercase
    # Usage: ./script.py < input.xml > output.xml
    #        ./script.py < input.xml | less

    import sys
    import xml.etree.ElementTree as ET

    # load
    tree = ET.parse(sys.stdin)

    # transform
    for element in tree.iter():
        element.tag = element.tag.lower()

    # save
    tree.write(sys.stdout)

    sys.exit(0)

La nostre feina consistirà en fer transformacions en els elements
recorreguts en el bucle que processa l’iterador retornat per
`tree.iter()`.

Enllaços recomanats
-------------------

-   Tutorial: [Elements and Element
    Trees](http://effbot.org/zone/element.htm)
-   [Python 2.5 Reference
    Card](http://home.uchicago.edu/~gan/file/python.pdf) (imprimeix en
    paper aquest document, i conserva una versió
    [local](../../UF2/A2/aux/python-2-5.pdf))
-   [Python Quick Reference Card](http://rgruet.free.fr/) (versió
    [local](../../UF2/A2/aux/python-QR.pdf))
-   Copia local de la [Python v2.7.5
    documentation](file:///usr/share/doc/python-docs/html/library/xml.etree.elementtree.html)
    (paquet RPM `python-docs`; vigila la versió del paquet per si ha
    canviat respecte a la de l’enllaç anterior)
-   Mòdul `xml.etree.ElementTree` en The Python Standard Library

Pràctiques I
------------

Pots fer aquestes pràctiques usant com a entrada aquest [catàleg de
plantes](aux/plant_catalog.xml).

Tots aquests exercicis consisteixen en modificar la transformació
[identitat](aux/identity.py), com fa per exemple la transformació a
[minúscules](aux/lower.py). Amb aquest esquema de treball, fes
transformacions per:

**Els exercicis es diran ex01.py, ex02.py, ...** i els posareu a dintre del directori CognomNom. Quan els tingueu, cal penjar el directori a dintre d'**Entregues**

1.   Eliminar tots els atributs d’un document.
2.   Afegir a tots els elements un atribut de nom `id` amb un valor
    diferent per cada element.
3.   Convertir a majúscules els noms d’atributs.
4.   Eliminar de tots els elements un determinat atribut, rebent el seu
    nom com argument (a `sys.argv[]`).
5.   Prefixar els noms dels elements amb una cadena, rebuda com argument
    (a `sys.argv[]`.
6.   Verificar que tots els elements fills de l’element arrel tenen un
    determinat nombre de fills, rebent aquest nombre com argument (a
    `sys.argv[])`.

Pràctiques II
-------------

En aquests exercicis la sortida dels programes ha de ser simple text, no
XML.

7.   Comptar el nombre de caràcters de text continguts en el
    document d’entrada.
8.   Comptar el nombre d’elements continguts en el document d’entrada.
9.   Comptar el nombre d’atributs continguts en el document d’entrada.
10.  Comptar el nombre d’elements fills de l’element arrel.
11.  Comptar el nombre d’elements sense fills (sense altres elements
    dins seu).
12.   Comptar el nombre d’elements que tenen fills (no son
    elements “terminals”).

Pràctiques II
-------------

13. Processa la sortida de l’ordre `sudo lshw -xml` per extreure parts de la
mateixa, presentar la informació de forma diferent, etc.
