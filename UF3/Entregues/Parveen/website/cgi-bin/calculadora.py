#!/usr/bin/python

'''Test CGI script.
'''

import sys
import cgi

#import cgitb; cgitb.enable()

# Debug
#import cgitb; cgitb.enable()

write = sys.stdout.write

form = cgi.FieldStorage()

# headers
write("Content-Type: text/html; charset=UTF-8\r\n") # or "text/html"...
write("\r\n")


print '<a href="http://localhost:8000/calculadora.html">back to calculater</a><br><br>'

if form.getvalue('num1') is None or not(str.isdigit(form.getvalue('num1'))):
	print 'error1'
	sys.exit(1)

if form.getvalue('num2') is None or not(str.isdigit(form.getvalue('num2'))):
	print 'error2'
	sys.exit(1)
	
if form.getvalue('num2') == '0' and form.getvalue('option') == '/' :
	print 'error 3'
	sys.exit(2)

number1 = int(form.getvalue('num1'))
number2 = int(form.getvalue('num2'))


if form.getvalue('option') == '/' :
	print number1 / number2
	
elif  form.getvalue('option') == '*' :
	print number1 * number2
elif  form.getvalue('option') == '-' :
	print number1 - number2

else :
	print number1 + number2
	
	
	
# body
#for k in form:
#    print k, "==>", form.getvalue(k)

sys.exit(0)

# vim:sw=4:ts=4:ai:et
