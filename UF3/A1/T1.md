Crear formularis
================

MP4UF3A1T1

Creació de formularis HMTL

Elements HTML per crear formularis
----------------------------------

Cal estudiar, en la documentació de referència del llenguatge HTML,
aquests elements:

-   [form](http://htmlhelp.com/reference/html40/forms/form.html) –
    Formulari interactiu
    -   [button](http://htmlhelp.com/reference/html40/forms/button.html)
        – Botó
    -   [fieldset](http://htmlhelp.com/reference/html40/forms/fieldset.html)
        – Grup de controls
        -   [legend](http://htmlhelp.com/reference/html40/forms/legend.html)
            – Llegenda per conjunt de camps
    -   [input](http://htmlhelp.com/reference/html40/forms/input.html) –
        Camp d’entrada
    -   [label](http://htmlhelp.com/reference/html40/forms/label.html) –
        Etiqueta per camp
    -   [select](http://htmlhelp.com/reference/html40/forms/select.html)
        – Selector d’opcions
        -   [optgroup](http://htmlhelp.com/reference/html40/forms/optgroup.html)
            – Grup d’opcions
            -   [option](http://htmlhelp.com/reference/html40/forms/option.html)
                – Opció en menú
    -   [textarea](http://htmlhelp.com/reference/html40/forms/textarea.html)
        – Entrada de text multilinia

Com es natural, validar els documents XHTML serà extremadament important
ara que estudiem aquesta nova part del vocabulari.

Enllaços recomanats
-------------------

-   [WDG HTML 4.0
    Reference](http://htmlhelp.com/distribution/wdghtml40.tar.gz)
    ([local](../../UF1/A3/aux/wdghtml40.tar.gz))
-   [XHTML Cheat
    Sheet](http://www.cheat-sheets.org/saved-copy/htmlcheatsheet.pdf)
    ([local](../../UF1/A3//aux/htmlcheatsheet.pdf))

Pràctiques
----------

Aquests exercicis demanen presentar diferents formularis, a fi
d’explorar les seves possibilitats. Tots els formularis han de ser
vàlids, tenir botons `submit` i `reset`, i ser processats per l’script
presentat al final de la pàgina.

-   Menú de restaurant, amb diferents grups de controls de tipus
    `radio`: primer plat, segon plat, postres i begudes.
-   Elecció de pizza (`radio`) i complements (`checkbox`).
-   Desplegable amb els noms de tots els països del mon.
-   Formulari per fer login.
-   Formulari per canvi de contrasenya.
-   Elecció de pizza amb desplegable de tipus `multiple`
    per complements.
-   Formulari amb dos botons `submit`, un per **Acceptar** i un altre
    per **Cancel·lar** (amb diferents `value`).
-   Formulari per enviar un missatge, amb camp `textarea` per cos
    del missatge.
-   Afegir al formulari anterior camps `hidden` per destinatari de
    missatge i nom de servidor SMTP.

Processament dels formularis
----------------------------

Informació tècnica (pots ignorar-la de moment): usarem de moment un
senzill servidor web que porta la llibreria de Python, que suporta CGIs
però no suporta en aquests la redirecció derivada d’usar la capçalera
`Location`.

Encara que ara ens preocupa principalment crear formularis, és natural
voler saber quina informació envia el navegador al servidor en prémer el
botó d’enviar el formulari. Per disposar d’un servidor web que presenti
la informació enviada pel navegador farem això:

-   Crearem un directori pels formularis:

        $ mkdir -p website/cgi-bin # directoris per formularis i scripts

-   Cada vegada que calgui arrencar el servidor web farem, en un
    terminal:

        $ cd website  # en aquest directori van els formularis HTML
        $ python /usr/lib64/python2.*/CGIHTTPServer.py  # ara llegeix la pantalla!

-   En un altre terminal ens situarem en el directori `website` per
    editar els formularis.
-   Per verificar el sistema, còpia el fitxer [form.py](aux/form.py) en
    el directori `cgi-bin` creat anteriorment (no t’oblidis de
    fer-lo executable). Alternativament pots usar aquest altre script:
    [form.sh](aux/form.sh).
-   El directori de treball (`website`) actua com arrel de les rutes en
    les URLs, i recorda que els scripts s’executen amb l’usuari `nobody`
    (això afecta als permisos!).
-   En tots els formularis l’element `form` serà així:
    `<form action="cgi-bin/form.py" ...>` .
-   Per visitar els formularis amb el navegador usarem l’adreça
    `http://localhost:8000/formulari`.

